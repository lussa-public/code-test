# Coding Test

for programmer candidate 

## How To

- fork this repositories into your own
- your time to solve this problem is 15 minutes
- you can write your solution using these tools

    - `golang` -> https://play.golang.org/
    - `javascript / typescript` -> https://jsbin.com/?js,console  / https://www.typescriptlang.org/play/

- write solutions in solutions directory (create a new folder named `solutions`)
- solution files should be using this formats `<problem_number>.<your tools extenstiion>` e.g. `1.ts`
- **no library allowed**

## Problems

1. Given a limited range of size `n` where array contains elements between `1` to `n-1` with one element repeating, find the duplicate number in it.

    **(5 points)**

    examples:
    ```javascript
    [1,2,3,4,4]
    the duplicate element is 4
    ```
    
    ```javascript
    [1,2,3,4,2]
    the duplicate element is 2
    ```

1. Given an array containing only `0`'s, `1`'s and `2`'s. sort the array in linear time and using constant space

    **(6 points)**

    examples:
    ```javascript
    [0,1,2,2,1,0,0,2,0,1,1,0]
    [0,0,0,0,0,1,1,1,1,2,2,2]
    ```

1. Given an array of integers find all distinct combinations of given length where repetition of elements is allowed

    **(6 points)**

    examples:
    ```javascript
    [1,2,3]
    [[1,1], [1,2], [1,3], [2,2], [2,3], [3,3]]
    ```

1. Given an array of integers, find maximum product (multiplication) subarray. In other words, find sub-array that has maximum product of its elements

    **(10 points)**

    examples:
    ```javascript
    [-6,4,-5,8,-10,0,8]
    the maximum product sub-array is [3,-5,8,-10] having product 1600
    ```
